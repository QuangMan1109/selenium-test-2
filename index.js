const express = require("express");

//init app
const app = express();
const MY_PORT = 3000;

const path = require("path");

const fs = require("fs");

//ejs
app.set("view engine", "ejs");
app.set("views", "./views");

//public folder
app.use(express.static("./public"));


//init socket io
const server = require("http").Server(app);
const io = require("socket.io")(server);
server.listen(process.env.PORT || MY_PORT);

io.on('connection', (socket) => {
    console.log(`There's someone connecting: ${socket.id}`);

    socket.on(`client-send-req`, async () => {
        console.log(`client-send-req`);
        await scrape({
            headless:   true,
        });

        console.log(`Server is responding`);
        socket.emit(`server-respond`);
    })
})

//get HOME PAGE
app.get("/", function(req, res) {
    res.render("home.ejs");
});


/*------------------ SCRAPE ------------------*/
const URL = `https://depositphotos.com/bgremover/upload.html`
const GMAIL_USERNAME = "duytran17.neu@gmail.com";
const GMAIL_PASSWORD = "YOURPASSWORD";
const selectors = {
    uploadInput:            "._2LnRd",
    downloadButton:         "._g6XNX._1RfBR._GNl8Q._3M5o-",
    signInButton:           ".signup-user-2020__login-link",
    signInWithGmailButton:  ".login-user__social-box div:nth-child(1)",
    usernameInput:          "input[type=email]",
    passwordInput:          "input[type=password",
    nextButtonHeadless:     "input[type=submit]",
    acceptButtonHeadless:   "#submit_approve_access",
    nextButtonNonHeadless:  ".VfPpkd-vQzf8d",
}

let downloadPath = path.resolve(`./public`);
// console.log(downloadPath);

let imgSrc = path.resolve(`./public/quang.jpg`);

const {Builder, By, Key, until, Capabilities} = require("selenium-webdriver");
require("chromedriver");
const chrome = require("selenium-webdriver/chrome");

let options = new chrome.Options();
options.setChromeBinaryPath(process.env.CHROME_BINARY_PATH)

options.setUserPreferences({
    "browser.set_download_behavior":   "allow",
    "download.default_directory":      downloadPath,
})

let serviceBuilder = new chrome.ServiceBuilder(process.env.CHROME_DRIVER_PATH);


let scrape = async ( {headless} ) => {

    if (headless) {

        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");

        let driver = await new Builder()
        .forBrowser("chrome")
        .setChromeOptions(options)
        .setChromeService(serviceBuilder)
        .build();


        //Go to the page
        await driver.get(URL);

        //Get window info
        let window1 = await driver.getWindowHandle();

        //Upload img
        await driver.findElement(By.css(selectors.uploadInput)).sendKeys(imgSrc)

        //Img1 for debugging
        let data = await driver.takeScreenshot();
        fs.writeFileSync(`./public/img1.png`, data, `base64`);


        //Click download button
        await driver.wait(until.elementLocated(By.css(selectors.downloadButton)));
        await driver.findElement( By.css(selectors.downloadButton) ).click();


        //Click sign in button
        await driver.wait(until.elementLocated(By.css(selectors.signInButton) ) );
        await driver.findElement( By.css(selectors.signInButton) ).click();

        //Click sign in with gmail button
        await driver.wait(until.elementLocated(By.css(selectors.signInWithGmailButton) ) );
        await driver.findElement( By.css(selectors.signInWithGmailButton) ).click();
        // let x2 = await driver.getWindowHandle();
        

        //Get all the window
        let windows = await driver.getAllWindowHandles();
        // console.log(windows);

        //Let driver switch to window2 (the window popping up)
        for (let window of windows) {
            if (window != window1) {
                await driver.switchTo().window(window)
            }
        }
        console.log(`-> Done switching to popping up window`);
        

        //Type your email
        await driver.sleep(3000);
        await driver.findElement(By.css(selectors.usernameInput)).sendKeys(GMAIL_USERNAME)
        console.log(`-> Done typing username`);

        //Click next button
        await driver.findElement(By.css(selectors.nextButtonHeadless)).click();
        console.log(`-> Done clicking next button`);


        //Img2 for debugging
        let data2 = await driver.takeScreenshot();
        fs.writeFileSync(`./public/img2.png`, data2, `base64`);
        console.log(`-> Done capturing img2`);
        

        //Type your password
        await driver.findElement(By.css(selectors.passwordInput)).sendKeys(GMAIL_PASSWORD);
        console.log(`-> Done typing password`);
        //Click next button
        await driver.findElement(By.css(selectors.nextButtonHeadless)).click();

        //Wait for navigation
        await driver.sleep(3000);
        console.log(`-> Done waiting for 3 seconds`);

        //Click accept button
        await driver.findElement(By.css(selectors.acceptButtonHeadless)).click();
        console.log(`-> Done clicking accept button and start downloading ...`);

        //It should automatically download the file like in non-headless mode, but it won't

        //Switch to initial window
        await driver.switchTo().window(window1);
    }

    else {
    
        let driver = await new Builder()
        .forBrowser("chrome")
        .setChromeOptions(options)
        .setChromeService(serviceBuilder)
        .build();
    
        //Go to the page
        await driver.get(URL);

        //Get window info
        let window1 = await driver.getWindowHandle();

        //Upload img
        await driver.findElement(By.css(selectors.uploadInput)).sendKeys(imgSrc)

        //Img1 for debugging
        let data = await driver.takeScreenshot();
        fs.writeFileSync(`./public/img1.png`, data, `base64`);


        //Click download button
        await driver.wait(until.elementLocated(By.css(selectors.downloadButton)));
        await driver.findElement( By.css(selectors.downloadButton) ).click();


        //Click sign in button
        await driver.wait(until.elementLocated(By.css(selectors.signInButton) ) );
        await driver.findElement( By.css(selectors.signInButton) ).click();

        //Click sign in with gmail button
        await driver.wait(until.elementLocated(By.css(selectors.signInWithGmailButton) ) );
        await driver.findElement( By.css(selectors.signInWithGmailButton) ).click();
        // let x2 = await driver.getWindowHandle();
        

        //Get all the window
        let windows = await driver.getAllWindowHandles();
        // console.log(windows);

        //Let driver switch to window2 (the window popping up)
        for (let window of windows) {
            if (window != window1) {
                await driver.switchTo().window(window)
            }
        }
        console.log(`-> Done switching to popping up window`);
    
        //Type username and click next button
        await driver.findElement(By.css(selectors.usernameInput)).sendKeys(GMAIL_USERNAME)        
        await driver.findElement(By.css(selectors.nextButtonNonHeadless)).click();
        console.log(`Done typing username and clicking next button`);
    
        let data2 = await driver.takeScreenshot();
        fs.writeFileSync(`./public/img2.png`, data2, `base64`);
    
        //Waiting for 3 seconds
        await driver.sleep(3000);
    
        //Type password and click next button
        await driver.findElement(By.css(selectors.passwordInput)).sendKeys(GMAIL_PASSWORD);
        await driver.findElement(By.css(selectors.nextButtonNonHeadless)).click();
        console.log(`Done typing password and clicking next button`);

        //It will automatically download the file
    }
} 

// scrape();